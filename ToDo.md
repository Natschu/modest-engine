# ToDo-List for modest-engine

## Very important ToDO's

- implement Game Engine Module Interface (see Game Engine Architecture by Gregory, Jason: Section 5.1.2)
- implement Game Loop (pattern) (see Game Programming Pattern by Nystorm, Robert: Section III.8)

## Current ToDo's

- add snippets for creating classes (.h and .cpp files)
- implement custom math library (see 1.)
- implement precompiled header
- implement typedef for characters/strings, and can this be combined with Unicode, etc.?
- implement Endian-Swapping (see Game Engine Architecture by Gregory, Jason: Section 3.2.1.6)
- implement flexible logger (static visibility), see "Chain Of Responsibility"-Pattern
- inform about precompiled headers

### Extended Details for ToDo's

1. math library

- vectors and scalars
- multiplication by scalars
- nonuniformal scalars
- addition and subtraction of vectors
- (square) magnitude
- normalize/normalization
- normal vector(perpendicular)
- dot product
- cross product
- lerp
- ...

## Finished ToDO's

- custom assertions class